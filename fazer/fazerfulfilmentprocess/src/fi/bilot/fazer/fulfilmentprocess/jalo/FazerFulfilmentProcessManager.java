/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package fi.bilot.fazer.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import fi.bilot.fazer.fulfilmentprocess.constants.FazerFulfilmentProcessConstants;

@SuppressWarnings("PMD")
public class FazerFulfilmentProcessManager extends GeneratedFazerFulfilmentProcessManager
{
	public static final FazerFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (FazerFulfilmentProcessManager) em.getExtension(FazerFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
