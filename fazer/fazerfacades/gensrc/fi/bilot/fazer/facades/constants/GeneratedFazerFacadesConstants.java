/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 16.10.2017 20:43:05                         ---
 * ----------------------------------------------------------------
 */
package fi.bilot.fazer.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFazerFacadesConstants
{
	public static final String EXTENSIONNAME = "fazerfacades";
	
	protected GeneratedFazerFacadesConstants()
	{
		// private constructor
	}
	
	
}
