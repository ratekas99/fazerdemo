/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package fi.bilot.fazer.facades.constants;

/**
 * Global class for all FazerFacades constants.
 */
@SuppressWarnings("PMD")
public class FazerFacadesConstants extends GeneratedFazerFacadesConstants
{
	public static final String EXTENSIONNAME = "fazerfacades";

	private FazerFacadesConstants()
	{
		//empty
	}
}
