/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 16.10.2017 20:43:05                         ---
 * ----------------------------------------------------------------
 */
package fi.bilot.fazer.cockpits.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFazerCockpitsConstants
{
	public static final String EXTENSIONNAME = "fazercockpits";
	
	protected GeneratedFazerCockpitsConstants()
	{
		// private constructor
	}
	
	
}
